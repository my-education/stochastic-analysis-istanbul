from datetime import datetime
import logging as log
import xlrd


class Reader:
	def __init__(self, file_name='data/Istanbul_StockMarket_09-11.'):
		self.file_name = file_name
		self.file_additional = 'data/neft.csv'
		self.titles = []
		self.data = []

	def read(self):
		log.info('Reading of the file: {}'.format(self.file_name))
		try:
			f = open(self.file_name + 'csv')
			titles = f.readline().replace('\n', '').split(';')
			data = []
			for line in f:
				is_first, obj = True, {}
				d = line.replace('\n', '').split(';')
				for i in range(len(titles)):
					obj[titles[i]] = d[i] if is_first else float(d[i])
					is_first = False
				data.append(obj)
			self.data = data
			self.titles = titles
			f.close()
		except OSError:
			f = xlrd.open_workbook(self.file_name + 'xlsx')

			original = self.read_xls_sheet(f, 'orjinal', 2)
			# date	ISE ISE SP DAX FTSE NIKKEI BOVESPA EU EM
			titles = list(filter(None, original.pop(0)))
			data = []
			for row_num in range(len(original)):
				line = {}
				for column in range(0, len(titles)):
					line[titles[column]] = original[row_num][column]
				data.append(line)
			self.data = data
			self.titles = titles
			self.write_csv()
		return self.data

	def read_xls_sheet(self, file, name: str, start_pos: int=0) -> list:
		sheet = file.sheet_by_name(name)
		data = []
		for nrow in range(start_pos, sheet.nrows):
			line = []
			for el in sheet.row(nrow):
				val = el.value
				if el.ctype == 3:
					py_date = datetime(*xlrd.xldate_as_tuple(el.value, file.datemode))
					val = py_date.strftime('%d-%m-%Y')
				line.append(val)
			data.append(line)
		return data

	def write_csv(self):
		f = open(self.file_name + 'csv', 'w')
		f.write(';'.join(self.titles) + '\n')
		for item in self.data:
			line = []
			for title in self.titles:
				line.append(str(item[title]))
			f.write(';'.join(line) + '\n')
		f.close()

	def read_additional(self, interpolate=True):
		f = open(self.file_additional, 'r')
		data = list(map(lambda v: float(v), f.readline().split(' ')))
		f.close()
		diff, prev = list(), data[0]
		for i in range(1, len(data)):
			diff.append(data[i] - prev)
			prev = data[i]

		# interpolate
		if interpolate:
			diff = self.interpolate(diff)

		return diff

	def interpolate(self, data, step=5):
		interpolate_data, inc = list(), 0
		for i in range(0, len(data)):
			interpolate_data.append(data[i])
			inc += 1
			if inc == 5 and data[i + 1]:
				interpolate_data.append(data[i] + ((data[i + 1] - data[i]) / 3))
				interpolate_data.append(data[i] + (((data[i + 1] - data[i]) / 3) * 2))
				inc = 0
		return interpolate_data
