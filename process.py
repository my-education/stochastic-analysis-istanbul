import numpy as np
import math
from scipy import signal
from plot import Plot


class Process:
	def __init__(self, data: list, column: str):
		self.data = data
		self.column = column

	def calculate_math_expectations(self, period: int, offset: str=0, column: str = None) -> list:
		if not column:
			column = self.column
		values, current, count = [], [], 0
		index = 0
		for instance in self.data:
			if index < offset:
				index += 1
				continue
			current.append(instance[column])
			count += 1
			if count >= period:
				values.append(np.mean(current))
				count, current = 0, []
		values.append(np.mean(current))
		return values

	def calculate_variances(self, period: int, offset: str=0, column: str = None) -> list:
		if not column:
			column = self.column
		values, current, count = [], [], 0
		index = 0
		for instance in self.data:
			if index < offset:
				index += 1
				continue
			current.append(instance[column])
			count += 1
			if count >= period:
				values.append(np.var(current))
				count, current = 0, []
		values.append(np.var(current))
		return values

	def calculate_autocorrelation(self) -> list:
		data = list(map(lambda x: x[self.column], self.data))
		res = list()
		for i in range(0, len(self.data) - 2):
			if i == 0:
				correlation = np.corrcoef(data, data)
			else:
				correlation = np.corrcoef(data[:-i], data[i:])
			correlation = correlation[0, 1]
			res.append(correlation)
		return self.cross_correlation(data, data)

	def cross_correlation(self, y, x=None):
		if not x:
			x = list(map(lambda v: v[self.column], self.data))
		res, N = list(), len(x)
		for tay in range(0, N - 100):
			# xm, ym = np.mean(x[1: N - tay]), np.mean(y[1 + tay: N - tay])
			# xd, yd = np.var(x[1: N - tay]), np.var(y[1 + tay: N - tay])
			correlation = np.corrcoef(x[1: N - tay], y[1 + tay:N])
			correlation = correlation[0, 1]
			# for t in range(1, N - tay):
			# 	correlation += (x[t] - xm) * (y[t + tay] - ym)
			# correlation = correlation * 19.5
			res.append(correlation)
		return res

	def spectrum_density(self, x=None, fs=10e3, window='bartlett'):
		"""

		:param x: stationary sequence
		:param fs:
		:param window: window for analysis, bartlett, hann, hamming, parzen
		:return:
		"""
		if not x:
			x = list(map(lambda v: v[self.column], self.data))

		fs = 1/ len(x)
		# f, Pxx_den = signal.periodogram(x, window=window)
		f, Pxx_den = signal.periodogram(x, fs=1, nfft=None, window=window)

		Plot.draw_semilogy(f, Pxx_den)

	@staticmethod
	def normalize(data, min_v = 0, max_v = 1):
		new_data = list()
		_max, _min = max(data), min(data)
		for val in data:
			new_data.append((val / _min) / (_max - _min))
		return new_data
