import matplotlib.pyplot as plt
from pandas import Series
import pandas as pd
from pandas.plotting import autocorrelation_plot
import logging as log


class Plot:

	def __init__(self, process):
		self.process = process

	def autocorrelation(self, column):
		data = list(map(lambda x: x[column], self.process.data))
		date = list(map(lambda x: x['date'].replace('-', '/'), self.process.data))

		ts = pd.Series(data)

		autocorrelation_plot(ts)
		plt.show()

	@staticmethod
	def draw_series(folder: str, interval: int, mean: list, variance: list):
		plt.plot(mean, '.-', label='Mean value')
		plt.plot(variance, '.-', label='Variances', color='r')
		plt.grid()
		plt.legend()
		plt.savefig('plots/{}/dt-{}.png'.format(folder, interval))
		plt.clf()

	@staticmethod
	def draw_array(rn: list, series: list, rn_m=None, means=None, rn_v=None, variances=None):
		plt.plot(rn, series, '-')
		if rn_m and means:
			plt.plot(rn_m, means, 'y-', label='lds model')
		if rn_v and variances:
			plt.plot(rn_v, variances, 'r-', label='Forecast')
		plt.grid()
		plt.legend()
		plt.show()

	@staticmethod
	def draw_semilogy(rn, series):
		plt.semilogy(rn, series)
		plt.grid()
		plt.show()

	@staticmethod
	def set_labels(title, x_label, y_label):
		plt.title(title)
		plt.xlabel(x_label)
		plt.ylabel(y_label)
