import numpy as np
import math


def nest_me(ef, f):
	_f = np.cumsum([0] + [f])
	EF = list()
	for l in range(len(_f) - 1):
		EF.append(ef[_f[l]:_f[l + 1]])
	return EF[0]


def cov(x, y, tau):
	mean_x, var_x = np.mean(x), np.var(x)
	mean_y, var_y = np.mean(y), np.var(y)
	N = len(x)
	res = np.array([(x[t] - mean_x) * (y[t + tau] - mean_y) for t in range(0, N - tau)]).mean() / (math.sqrt(var_x * var_y))
	return res


def auto_cov(data, tau):
	"""Calculation the auto covariance for input data and specified shift"""
	mean = np.mean(data)
	N = len(data)
	return np.array([(data[t] - mean) * (data[t + tau] - mean) for t in range(0, N - tau)]).mean()


def calculate_ef(x, order=5):
	N = len(x)
	right = []
	for i in range(1, order + 1):
		right.append([np.corrcoef(x[0: N - abs(j - i)], x[abs(j - i):N])[0, 1] for j in range(1, order + 1)])
	left = [np.corrcoef(x[0: N - i], x[0 + i:N])[0, 1] for i in range(1, order + 1)]
	return np.linalg.solve(right, left)


def parameters_lsd(x, y, f_order=5):
	ef = calculate_ef(x, order=f_order)
	ef_nested = nest_me(ef, f_order)
	left_sum = np.array([cov(x, y, tau=i) for i in range(1, f_order + 1)])
	s1 = [[cov(x, y, tau=abs(j - i)) for j in range(1, f_order + 1)] for i in range(1, f_order + 1)]
	auto_sum = np.tensordot(s1, ef_nested, axes=((-1), (0)))
	left = left_sum - auto_sum
	s2, N = [], len(x)
	for i in range(1, f_order + 1):
		s2.append([np.corrcoef(y[0: N - abs(j - i)], y[abs(j - i):N])[0, 1] for j in range(1, f_order + 1)])
	ksi = np.linalg.solve(s2, left)
	noise_sigma = np.corrcoef(x, x)[0, 1] - sum([sum(ef[i - 1] * np.corrcoef(x[0:N-i], x[i:N])[0, 1] for i in range(1, f_order + 1))])
	noise = np.random.uniform(-noise_sigma, noise_sigma, len(x) + 100)
	return ef, ksi, noise


def lds_model(x, y, f_order=5, cnt_pred=26):
	ef, ksi, noise = parameters_lsd(x, y, f_order=f_order)
	ef, ksi= np.array(ef), np.array(ksi)
	full_data = np.array(x[0:len(x) - cnt_pred])
	N = len(full_data)
	res_auto, res_factors = np.zeros(len(x) - cnt_pred - f_order), np.zeros(len(x) - cnt_pred - f_order)
	for t in range(0, f_order):
		res_auto += np.array(ef[t] * full_data[f_order - t:N-t])
		res_factors += np.array(ksi[t] * full_data[f_order - t:N-t])
	full_data[f_order:] = res_auto + res_factors
	full_data = list(full_data)

	pred = x[:len(x) - (f_order - 1)]
	for t in range(len(x) - cnt_pred, len(x)):
		res_auto = sum([ef[i] * pred[t - i - 1] for i in range(f_order)])
		res_factors = sum([ksi[i] * pred[t - i - 1] for i in range(f_order)])
		full_data.append(res_auto + res_factors + noise[t])
		pred.append(res_auto + res_factors + noise[t])
	return full_data[:len(x) - cnt_pred], full_data[len(x) - cnt_pred:]
