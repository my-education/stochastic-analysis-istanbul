## Execution steps

**For path 1 of the task:**

1. To analyze stationarity of a process (for mathematical expectation and variance)
1. To analyze covariance function. To define covariance (or correlation window)
1. To estimate spectral density function with using different functions for spectral window
1. To filter high frequencies (noise) with using various filters (e.g. moving average, Gaussian filter)
1. To repeat estimation of spectral density and compare with result for non-filtered data
1. To built Rice model for process (using filtered and non-filtered data)
1. To built auto-regression model filtered and non-filtered data. To analyze residual error and to define appropriate order of model

**For path 2 of the task:**

1. To find additional factors that influence on chosen index (e.g. oil prices, gold prices, number of deaths, other indexes etc.)
1. To analyze mutual correlation functions among factors
1. To build model in a form of linear dynamical system, using additional factors. To analyze residual error and to define appropriate order of model
