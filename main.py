from reader import Reader
from process import Process
from plot import Plot
import logging as log
import numpy as np
import cv2 as cv
from statsmodels.tsa.ar_model import AR
from path2 import *


formatter = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s'
log.basicConfig(format=formatter, level=log.DEBUG)

#  read data
reader = Reader()
data = reader.read()
additional_data = reader.read_additional()

# initialize process
process = Process(data, 'ISE')

#  init plot
plt = Plot(process)


def step_1():
	for dt in range(40, 201, 40):
		means = process.calculate_math_expectations(dt)
		variances = process.calculate_variances(dt)
		Plot.set_labels('First analysis for means and variances. dt={}'.format(dt), 'days', 'value')
		Plot.draw_series('step_1', dt, means, variances)


def mean_variance(data: list, dt: int):
	means, variances = list(), list()
	rn = list(range(0, len(data), dt))
	for i in rn:
		means.append(np.mean(data[i: i + dt]))
		variances.append(np.var(data[i: i + dt]))
	return means, variances


def step_2(pandas_column=None):
	rn = list(range(1, int(len(data) * 0.99)))
	autocorrelation = process.calculate_autocorrelation(0, rn)
	Plot.set_labels('Custom autocorrelation', 'Shift', 'Value')
	Plot.draw_array(rn, autocorrelation)
	if pandas_column:
		plt.autocorrelation(pandas_column)
	d = list(map(lambda x: x[process.column], data))
	result = np.correlate(d, d, 'full')
	result = result[int(result.size / 2):]

	# Plot.set_labels('Autocorrelation using nupmy', 'Shift', 'Value')
	Plot.set_labels('Autocorrelation', 'Shift', 'Value')
	Plot.draw_array(list(range(1, len(result))), result[1:])


# step_2()
# process.spectrum_density()

# Отрисовка среднего значения и дисперсии для нефти
add_data_norm = Process.normalize(additional_data)
dt = 30
means, variances = mean_variance(add_data_norm, dt)
rn = list(range(0, len(add_data_norm), dt))
Plot.set_labels('Mean and Variance for additional data', 'Days', 'Values')
# Plot.draw_array(list(range(0, len(add_data_norm))), add_data_norm, rn, means, variances)


# Автокорреляция для нефти
result = np.correlate(add_data_norm, add_data_norm, 'full')
result = result[int(result.size / 2):]
# result = process.cross_correlation(add_data_norm, add_data_norm)

Plot.set_labels('Autocorrelation', 'Shift', 'Value')
# Plot.draw_array(list(range(0, len(result))), result)

# Корреляция исходных данных с Нефтью
data_inter = reader.interpolate(list(map(lambda x: x[process.column], data)))
data_inter = data_inter[:726]

# result = np.correlate(list(map(lambda x: x[process.column], data))[10:], Process.normalize(reader.read_additional(False)), 'full')
# result = result[int(result.size / 2):]
result = process.cross_correlation(add_data_norm, data_inter)

Plot.set_labels('Cross correlation', 'Shift', 'Value')
# Plot.draw_array(list(range(0, len(result))), result)

# Определение порядков для data_inter add_data_norm

order = 15
errors = []
for o in range(3, 20):
	x, forecast = lds_model(data_inter, add_data_norm, o)
	error = (np.array(data_inter[:700]) - np.array(x)).mean()
	# error += (np.array(data_inter[700:]) - np.array(forecast)).mean()
	errors.append(error)
plt.draw_array(list(range(3, 20)), errors)

x, forecast = lds_model(data_inter, add_data_norm, order)
plt.set_labels('Mean orders', 'Order', 'Mean error')

# plt.draw_array(list(range(500, 726)), data_inter[500:], list(range(500, 726)), None, list(x[500:]))
plt.draw_array(list(range(726)), data_inter, list(range(len(x))), x, list(range(len(x) - 1, len(x) - 1 + len(forecast))), forecast)
print(1)